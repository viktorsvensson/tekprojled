package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    public void setup(){
        calculator = new Calculator();
    }

    @Test
    public void add_withIntegers_shouldReturnSum(){
        // Given
        int a = 5;
        int b = 2;

        // When
        int result = calculator.add(a, b);

        // Then
        assertEquals(7, result);

    }

    @ParameterizedTest
    @CsvSource(value = {"2, 4", "-2, 4", "0, 0"})
    public void squared_withPositiveNumbers_shouldReturnProduct(int x, int expected){
        // Given


        // When
        int result = calculator.squared(x);

        // Then
        assertEquals(expected, result);
    }
/*
    @Test
    public void squared_withNegativeNumbers_shouldReturnProduct(){
        // Given
        int x = -2;
        Calculator calculator = new Calculator();

        // When
        int result = calculator.squared(x);

        // Then
        assertEquals(4, result);
    }
 */

    @Test
    public void divide_whenDivisorIsZero_shouldThrow(){
        // Given
        int x = 5;
        int y = 0;

        // When
        ArithmeticException e =
                assertThrows(ArithmeticException.class, () -> calculator.divide(x, y));
        // Then
        assertEquals("/ by zero", e.getMessage());

    }
}
