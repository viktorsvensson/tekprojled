package org.example;

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public int squared(int x) {
        return x * x;
    }

    public int divide(int x, int y) {
        return x / y;
    }
}
